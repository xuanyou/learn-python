# learn-python

- How to use IDLE Python GUI
- How to handle ModuleNotFoundError: No module named 'requests' in Windows
- Concept of request and response
- How to read JSON
- Basic Python: Variable, String, Dict, List
- Searching in Dict
- Putting it all together to query weather forecast
